This project includes a small service that orders devices (I've only included realisation of the service itself without its API).

Imagine that this is a world where all countries are divided in two categories: those with odd numbers and those with even.
People from odd countries like USA can only use our odd devices and people from even countries like Great Britain can 
only use even ones. In this case, we don't want to allow people to purchase wrong device by accident, so we should
throw error.

I've added service that validates and manages purchases, a number of unit tests and two integration tests. Since you've
asked to show code close to my real project, I've kept REST Assured as we use. I also think that if we already have unit 
tests that cover the cases, we don't need 4 integration tests for this. I would only leave without testing different
combinations with DataProvider to check correct status codes and messages.

Please also note that I know that no real POST request should look like that. I just got tired and didn't want to serialize
object with deviceId into json with Jackson ObjectMapper to send deviceId as body or make some hideous String concatenation.

P.S. unit tests actually work and pass, all project can be built.