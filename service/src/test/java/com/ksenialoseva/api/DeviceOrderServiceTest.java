package com.ksenialoseva.api;

import com.ksenialoseva.api.model.User;
import org.junit.Test;

/**
 * In this case we don't need mocks but in case of real billing service and
 * real user storage we would mock their behaviour.
 */
public class DeviceOrderServiceTest {

    BillingService billingService = new BillingService();
    UserStorage userStorage = new UserStorage();

    private DeviceOrderService deviceOrderService = new DeviceOrderService(billingService, userStorage);

    @Test
    public void shouldAllowPurchasingCorrectDevice() throws BillingService.ForbiddenDeviceException {
        User user = userStorage.getUserById(1234L);
        deviceOrderService.orderDevice(user.getUserId(), 1234L);
    }

    @Test(expected = BillingService.ForbiddenDeviceException.class)
    public void shouldFailPurchasingOddDeviceForEvenUser() throws BillingService.ForbiddenDeviceException {
        User user = userStorage.getUserById(1234L);
        deviceOrderService.orderDevice(user.getUserId(), 12345L);
    }

    @Test(expected = BillingService.ForbiddenDeviceException.class)
    public void shouldFailPurchasingEvenDeviceForOddUser() throws BillingService.ForbiddenDeviceException {
        User user = userStorage.getUserById(12345L);
        deviceOrderService.orderDevice(user.getUserId(), 1234L);
    }

}