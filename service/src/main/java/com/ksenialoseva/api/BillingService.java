package com.ksenialoseva.api;

import com.ksenialoseva.api.model.User;

public class BillingService {

    public void validateUserPurchase(User user, Long deviceId) throws ForbiddenDeviceException {
        if (user.getCountry().equals(0L) && deviceId % 2 == 1) {
            throw new ForbiddenDeviceException(user.getCountry(), deviceId);
        } else if (user.getCountry().equals(1L) && deviceId % 2 == 0) {
            throw new ForbiddenDeviceException(user.getCountry(), deviceId);
        }
    }

    public void purchaseDevice(User user, Long deviceId) {

    }

    public class ForbiddenDeviceException extends Throwable {
        public ForbiddenDeviceException(Long country, Long deviceId) {
            super(String.format("user from country %s can't purchase device %s", country, deviceId));
        }

    }
}
