package com.ksenialoseva.api;

import com.ksenialoseva.api.model.User;

public class UserStorage {

    public User getUserById(Long userId) {
        return new User(userId);
    }
}
