package com.ksenialoseva.api.model;

import lombok.Data;

@Data
public class User {
    private final Long userId;
    private final Long country;

    public User(Long userId) {
        this.userId = userId;
        this.country = userId % 2;
    }
}
