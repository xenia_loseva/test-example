package com.ksenialoseva.api;

import com.ksenialoseva.api.model.User;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DeviceOrderService {

    private final BillingService billingService;
    private final UserStorage userStorage;

    public void orderDevice(Long userId, Long deviceId) throws BillingService.ForbiddenDeviceException {
        User user = userStorage.getUserById(userId);
        billingService.validateUserPurchase(user, deviceId);
        billingService.purchaseDevice(user, deviceId);
    }
}
