package com.ksenialoseva.api.model;

import lombok.Data;

@Data
public class User {
    private Long userId;
    private Long country;
    private String password = "qwerty12345";

    public User(Long userId, Long country) {
        this.userId = userId;
        this.country = country;
    }

    public String getId() {
        return String.valueOf(userId);
    }

}
