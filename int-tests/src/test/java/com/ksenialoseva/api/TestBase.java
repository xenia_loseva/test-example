package com.ksenialoseva.api;

import com.ksenialoseva.api.helper.AuthorizationFilter;
import io.restassured.RestAssured;
import io.restassured.filter.Filter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.testng.annotations.BeforeMethod;

import java.util.Arrays;
import java.util.List;

public class TestBase {

    @BeforeMethod
    public void setUpRestAssured() {
        RestAssured.baseURI = "https://example.com";
        List<Filter> filters = Arrays.asList(
                new AuthorizationFilter(),
                new RequestLoggingFilter(),
                new ResponseLoggingFilter()
        );
        RestAssured.filters(filters);
    }
}
