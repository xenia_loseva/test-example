package com.ksenialoseva.api.helper;

import com.ksenialoseva.api.model.User;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_OK;

/**
 * This is how Rest Assured is used in our tests. I think that this is
 * definitely not the most efficient way to use it and would probably choose
 * Retrofit over it but I leave it as it is just because you
 * asked me to show how I write tests. But you can imagine cool Retrofit interface
 * here instead.
 */
public class RequestHelper {

    public static void authAsUser(User user) {
        String token = given()
                .auth().preemptive().basic(user.getId(), user.getPassword())
                .post("/token")
                .then()
                .statusCode(HTTP_OK)
                .extract()
                .jsonPath()
                .getString("access_token");

        AuthorizationFilter.setToken(token);
    }

    public static JsonPath successOrderDevice(String deviceId) {
        return given()
                .post("/device")
                .then()
                .statusCode(HTTP_OK)
                .extract()
                .jsonPath();
    }

    public static JsonPath failOrderDevice(String deviceId) {
        return given()
                .post("/device")
                .then()
                .statusCode(HTTP_FORBIDDEN)
                .extract()
                .jsonPath();
    }
}
