package com.ksenialoseva.api.helper;

import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import io.restassured.spi.AuthFilter;

import static org.apache.http.HttpHeaders.AUTHORIZATION;

public class AuthorizationFilter implements AuthFilter {
    private static ThreadLocal<String> token = ThreadLocal.withInitial(() -> "");

    public static void setToken(String token) {
        AuthorizationFilter.token.set(token);
    }

    @Override
    public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec, FilterContext ctx) {
        if (!token.get().isEmpty()) {
            requestSpec = requestSpec.replaceHeader(AUTHORIZATION, token.get());
        }
        return ctx.next(requestSpec, responseSpec);
    }
}
