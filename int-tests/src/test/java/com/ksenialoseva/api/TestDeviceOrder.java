package com.ksenialoseva.api;


import com.ksenialoseva.api.helper.RequestHelper;
import com.ksenialoseva.api.helper.UserHelper;
import com.ksenialoseva.api.model.User;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestDeviceOrder extends TestBase {

    private static final Long USA_ID = 1L;
    private static final Long GB_ID = 2L;

    private static final String USA_DEVICE_ID = "10";
    private static final String GB_DEVICE_ID = "15";

    private static final String FORBIDDEN_ERROR_CODE = "FBDN_DEVICE";

    @DataProvider(name = "countriesWithCorrectDevices")
    public Object[][] countriesWithCorrectDevices() {
        return new Object[][]{
                {USA_ID, USA_DEVICE_ID},
                {GB_ID, GB_DEVICE_ID}
        };
    }

    @Test(dataProvider = "countriesWithCorrectDevices")
    public void shouldSuccessfullyPurchaseCorrectDevice(Long country, String deviceId) {
        User user = UserHelper.getUserWithCountry(country);
        RequestHelper.authAsUser(user);

        RequestHelper.successOrderDevice(deviceId);
    }

    @DataProvider(name = "countriesWithIncorrectDevices")
    public Object[][] countriesWithIncorrectDevices() {
        return new Object[][]{
                {USA_ID, GB_DEVICE_ID},
                {GB_ID, USA_DEVICE_ID}
        };
    }

    @Test(dataProvider = "countriesWithCorrectDevices")
    public void shouldFailPurchaseCorrectDevice(Long country, String deviceId) {
        User user = UserHelper.getUserWithCountry(country);
        RequestHelper.authAsUser(user);

        JsonPath failedResponse = RequestHelper.failOrderDevice(deviceId);
        assertThat(failedResponse.getString("errorMessage"))
                .as("error message should tell user that they cannot purchase the device")
                .isEqualTo("It is forbidden to order this device for user from this country");

        assertThat(failedResponse.getString("errorCode"))
                .as("error code should match expected by specification")
                .isEqualTo(FORBIDDEN_ERROR_CODE);

    }
}
